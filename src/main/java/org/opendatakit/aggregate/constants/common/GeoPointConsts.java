package org.opendatakit.aggregate.constants.common;

public class GeoPointConsts {

  // GEOPOINT structured field has the following ordinal interpretations...
  public static final int GEOPOINT_LATITUDE_ORDINAL_NUMBER = 1;
  public static final int GEOPOINT_LONGITUDE_ORDINAL_NUMBER = 2;
  public static final int GEOPOINT_ALTITUDE_ORDINAL_NUMBER = 3;
  public static final int GEOPOINT_ACCURACY_ORDINAL_NUMBER = 4;

}
