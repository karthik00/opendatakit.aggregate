package org.opendatakit.aggregate.constants.common;

public interface HelpSliderConsts {
  public String getTitle();

  public String getContent();
}
