package org.opendatakit.aggregate.util;

public interface ImageUtil {

  public byte[] resizeImage(byte[] imageBlob, int width, int height);
  
}
