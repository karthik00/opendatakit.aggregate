package org.opendatakit.aggregate.odktables.client.entity;

public enum FilterOperation {
    EQUAL, NOT_EQUAL, GREATER_THAN, GREATER_THAN_OR_EQUAL, LESS_THAN, LESS_THAN_OR_EQUAL;
}
