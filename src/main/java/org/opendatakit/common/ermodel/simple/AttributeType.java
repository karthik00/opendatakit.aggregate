package org.opendatakit.common.ermodel.simple;

public enum AttributeType
{
    STRING, INTEGER, DECIMAL, BOOLEAN, DATETIME,
}
